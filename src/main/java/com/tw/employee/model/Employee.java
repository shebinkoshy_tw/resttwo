package com.tw.employee.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Table(name="employee") // annotation will map our class to the corresponding database table
@Data
public class Employee {

//    @Id @GeneratedValue(strategy = GenerationType.AUTO) // primary key of the Entity
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "employee_id", nullable = false, unique = true)
    private String employeeId;

    @NotNull(message = "FirstName cannot be null")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull(message = "LastName cannot be null")
    @Column(name = "last_name", nullable = false)
    private String lastName;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "address_id", unique = true, nullable = false)
//    private int addressId;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "contact_id", unique = true, nullable = false)
//    private int contactId;

//    @OneToOne(mappedBy = "employee")
//@Getter(AccessLevel.NONE)
//@Setter(AccessLevel.NONE)
//    @OneToOne(mappedBy = "employee",  cascade = CascadeType.ALL)
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

//    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL) // any operation in general happens on a single entity
    // to allow JPA operate / act on multiple entities or To establish a dependency between related entities,
    // JPA provides javax.persistence.CascadeType enumerated types that define the cascade operations
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "contact_id", referencedColumnName = "id")
    private Contact contact;


   /* @JsonManagedReference
    @OneToMany(mappedBy = "employee")
    private List<Recommendation> recommendations;*/

    public Employee() {
    }

    public Employee(String id, String firstName, String lastName) {
        this.employeeId = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
