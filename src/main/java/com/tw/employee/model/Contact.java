package com.tw.employee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="contact")
@Data
public class Contact {

//    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "email_id", nullable = false)
    private String emailId;

    @Column(name = "phone_number", nullable = false)
    private long phoneNumber;

//    @OneToOne(targetEntity = Employee.class)
//    @JoinColumn(name="contact_id", referencedColumnName = "id", insertable = false, nullable=false)
@JsonIgnore
@OneToOne(mappedBy = "contact")
    private Employee employee;

}
