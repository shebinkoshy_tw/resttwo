package com.tw.employee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name="address")
@Data
public class Address {

//    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state", nullable = false)
    private String state;

    @Column(name = "zip_code", nullable = false)
    private String zipCode;

//    @OneToOne(targetEntity = Employee.class)//(cascade = CascadeType.ALL)
//    @JoinColumn(name="address_id", referencedColumnName = "id", insertable = false, nullable=false)
@JsonIgnore
@OneToOne(mappedBy = "address")
    private Employee employee;


}
