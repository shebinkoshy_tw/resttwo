package com.tw.employee.repository;


import com.tw.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EmployeeJPARepository extends EmployeeRepository, JpaRepository<Employee, Integer> {
    Employee findByEmployeeId(String employeeId);
    Employee findByEmployeeIdLike(String employeeId);

    @Query("from Employee e where e.firstName like %?1%") // Talk about positional params and :name
    List<Employee> findByFirstName(String firstName);

//    @Transactional
    @Modifying
    @Query("update Employee e set e.firstName = :firstName, e.lastName = :lastName where e.employeeId = :employeeId")
    void updateEmployeeById(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("employeeId") String employeeId);


}
